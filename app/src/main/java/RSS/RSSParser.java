package RSS;

import android.text.Html;
import android.text.Spannable;
import android.text.Spanned;
import android.util.Log;
import android.util.Xml;

import org.xml.sax.XMLReader;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.StringReader;

/**
 * Created by ant94 on 19.05.2016.
 */
public class RSSParser {

    String RssPars(String RssString)
            throws XmlPullParserException, IOException {
        String newsString = "";
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        factory.setNamespaceAware(true);
        XmlPullParser xpp = factory.newPullParser();

        xpp.setInput(new StringReader(RssString));
        int eventType = xpp.getEventType();
        while (eventType != XmlPullParser.END_DOCUMENT) {
            if (eventType == XmlPullParser.START_DOCUMENT) {
                newsString+="\n";
            } else if (eventType == XmlPullParser.START_TAG) {
                newsString+="\n";
            } else if (eventType == XmlPullParser.END_TAG) {
                newsString+="\n";
            } else if (eventType == XmlPullParser.TEXT) {
                newsString +="\n   " + xpp.getText();
            }
            eventType = xpp.next();
        }
        return newsString;
    }

    final Spannable revertSpanned(Spanned stext) {
        Object[] spans = stext.getSpans(0, stext.length(), Object.class);
        Spannable ret = Spannable.Factory.getInstance().newSpannable(stext.toString());
        if (spans != null && spans.length > 0) {
            for (int i = spans.length - 1; i >= 0; --i) {
                ret.setSpan(spans[i], stext.getSpanStart(spans[i]), stext.getSpanEnd(spans[i]), stext.getSpanFlags(spans[i]));
            }
        }

        return ret;
    }

    public String encoding(String rssString){

        return rssString;
    }

    public final Spannable GetRssSpan(final String rssString) {
        Spanned spanned = null;
        try {
            spanned = Html.fromHtml(new String(RssPars(rssString))/*,new Html.ImageGetter() {

                @Override
                public Drawable getDrawable(String source) {
                    new ImageDownloadAsyncTask().execute((textView)123, rssString, source);
                    return null;
                }
            },null*/);
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Spannable reversedText = revertSpanned(spanned);
        return reversedText;
    }

}
