package com.bomjsoft.bomjnews;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import RSS.RSSParser;
import database.RSSList;

public class ReadActivity extends AppCompatActivity {
    RSSList rssList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read);

        final Intent intent = getIntent();
        rssList = new RSSList(intent.getIntExtra("id", 1), intent.getStringExtra("title"),
                intent.getStringExtra("path"));

        final TextView textView = (TextView) findViewById(R.id.textView);
Log.i("Activ","Text view ща");
        textView.setText("ID: " + String.valueOf(rssList.getId()) + "\nTitle: " + rssList.getTitle() + "\nPath: " + rssList.getPath() + "\n\n\n" + new RSSParser().GetRssSpan(ReadFile(rssList)));//+RssPars(ReadFile(rssList)));//+ ReadFile(rssList));

    }

    public String ReadFile(RSSList rssList) {
        try {
            // открываем поток для чтения
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    openFileInput(String.valueOf(rssList.getId()) + ".xml")));
            String str = "";
            // читаем содержимое
            while ((str = br.readLine()) != null) {
                Log.d("readFile", str);
                return str;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return "<rss>Файл не найден.</rss>";
        } catch (IOException e) {
            e.printStackTrace();
            return "<rss>Чтото пошло не так.</rss>";
        }
        return "<rss>Хм...</rss>";
    }
}
