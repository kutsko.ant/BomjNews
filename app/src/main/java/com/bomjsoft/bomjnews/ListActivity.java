package com.bomjsoft.bomjnews;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.concurrent.ExecutionException;

import RSS.TLSDEmo;
import database.DBHelper;
import database.RSSList;

public class ListActivity extends AppCompatActivity {

    DBHelper dbHelper;
    ListView contentList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        dbHelper = new DBHelper(this);
        contentList = (ListView)findViewById(R.id.contentList);
        CreateListNotes();

        FloatingActionButton deleteButton = (FloatingActionButton) findViewById(R.id.DeleteButton);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("MainActivity", "--- deleteButton Start(Checked item position =" + contentList.getCheckedItemPosition() + ") ---");
                if (contentList.getCheckedItemPosition() >= 0) {
                    RSSList rss = (RSSList)contentList.getItemAtPosition(contentList.getCheckedItemPosition());
                    Log.i("MainActivity", "--- deleteButton(Delere Note id =" + rss.getId() + ") ---");
                    dbHelper.DeleteNote(rss.getId());
                    deleteFile(String.valueOf(rss.getId() + ".xml"));
                    Log.i("MainActivity", "--- Файл" + rss.getId() + ".xml"+" удалён ---");
                            Snackbar.make(view, "Item delete", Snackbar.LENGTH_SHORT).setAction("Action", null).show();
                } else
                    Snackbar.make(view, "No selected item", Snackbar.LENGTH_SHORT).setAction("Action", null).show();
                Log.i("MainActivity", "--- deleteButton Finish ---");
                CreateListNotes();
            }
        });

        FloatingActionButton openButton = (FloatingActionButton) findViewById(R.id.OpenButton);
        openButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.i("MainActivity", "--- openButton Start ---");
                if (contentList.getCheckedItemPosition() >= 0) {
                    RSSList rssList = (RSSList) contentList.getItemAtPosition(contentList.getCheckedItemPosition());
                    Log.i("MainActivity", "--- openButton(Open rssList id =" + rssList.getId() + ") ---");
                    Intent intent = new Intent(ListActivity.this, ReadActivity.class);
                    intent.putExtra("id", (int) rssList.getId());
                    intent.putExtra("title", rssList.getTitle());
                    intent.putExtra("path", rssList.getPath());
                    startActivityForResult(intent, 1);
                } else
                    Snackbar.make(view, "No selected item", Snackbar.LENGTH_SHORT).setAction("Action", null).show();
                Log.i("MainActivity", "--- openButton Finish ---");
            }
        });




        final EditText editTitle = (EditText) findViewById(R.id.titleEdit);
        final EditText editPath = (EditText) findViewById(R.id.pathEdit);

        FloatingActionButton addButton = (FloatingActionButton) findViewById(R.id.AddButton);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dbHelper.AddRSSList(editTitle.getText().toString(), editPath.getText().toString());
                Snackbar.make(view, "New item add", Snackbar.LENGTH_SHORT).setAction("Action", null).show();
                editTitle.setText("");
                editPath.setText("");
                CreateListNotes();
            }
        });

        FloatingActionButton refreshButton = (FloatingActionButton) findViewById(R.id.RefreshButton);
        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("MainActivity", "--- RefreshButton Start ---");
                if (contentList.getCheckedItemPosition() >= 0) {
                    WriteRSStoFile(view, (RSSList) contentList.getItemAtPosition(contentList.getCheckedItemPosition()));
                } else
                    Snackbar.make(view, "No selected item", Snackbar.LENGTH_SHORT).setAction("Action", null).show();
                Log.i("MainActivity", "--- RefreshButton Finish ---");
            }
        });

        contentList.setFocusable(true);
        //Log.w("READRSS", ReadRSS("https://people.onliner.by/feed"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_list, menu);
        return true;
    }

    public void CreateListNotes(){
        Log.i("MainActivity", "--- CreateListNotes Start ---");
        ArrayAdapter<RSSList> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_single_choice, dbHelper.GetAllNotes());
        contentList.setAdapter(adapter);
        Log.i("MainActivity", "--- CreateListNotes Finish ---");
    }

//-------------------------------------------------------------
public String ReadRSS(String urlString){
    try {
        Log.i("READ RSS", "--- READ START ---");
        return new TLSDEmo().execute(urlString).get();
    } catch (InterruptedException | ExecutionException e) {
        e.printStackTrace();
    }
    return "";
}

    public void WriteRSStoFile(View view,RSSList rssList) {
        if (ReadRSS(rssList.getPath()) != null) {
            String rssString = "" + ReadRSS(rssList.getPath());
            Log.i("WriteRSStoFile", "--- WriteRSStoFile START ---");
            try {
                // отрываем поток для записи
                Log.i("WriteRSStoFile", "Имя: " + String.valueOf(rssList.getId()) + ".xml Строка: " + rssString);
                BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
                        openFileOutput(String.valueOf(rssList.getId() + ".xml"), MODE_PRIVATE)));
                // пишем данные
                bw.write(rssString);
                // закрываем поток
                bw.close();
                Log.d("writeFile", rssString);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Log.i("WriteRSStoFile", "--- Файл не найден! ---");
            } catch (IOException e) {
                e.printStackTrace();
                Log.i("WriteRSStoFile", "--- Чтото явно пошло не так. ---");
                Snackbar.make(view, "Refresh item", Snackbar.LENGTH_SHORT).setAction("Action", null).show();
            }
        }Snackbar.make(view, "Url string=null", Snackbar.LENGTH_SHORT).setAction("Action", null).show();
    }

//---------------------------------------------------------------------------

}
