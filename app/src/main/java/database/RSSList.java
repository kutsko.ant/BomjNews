package database;

/**
 * Created by ant94 on 14.04.2016.
 */
public class RSSList {
    private int id;
    private String title;
    private String path;

    public RSSList(int id, String title, String path)
    {
        this.id=id;
        this.title=title;
        this.path=path;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String content) {
        this.path = path;
    }

    @Override
    public String toString() {
        return SubStr(getTitle()+" - "+getPath());
    }

    public static String SubStr(String text) {
        return text.length() <= 35 ? text : text.substring(0, 35) + "...";
    }
}
