package database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by ant94 on 14.04.2016.
 */
public class DBHelper extends SQLiteOpenHelper {
    public DBHelper(Context context) {
        super(context, databaseName, null, databaseVersion);
    }
    private static final String databaseName="DBRSS.db";
    private static final int databaseVersion=1;

    private static final String RSSTableName ="RSSLists";
    private static final String idColumnName="id";
    private static final String titleColumnName="title";
    private static final String pathColumnName="path";
    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.v("DBHelper", "--- onCreate Start ---");

        db.execSQL("create table " + RSSTableName + " ("
                + idColumnName + " integer primary key autoincrement,"
                + titleColumnName + " text,"
                + pathColumnName + " text );");
        Log.v("DBHelper", "--- onCreate Finish ---");
    }

    public void UpdateNote(int id,String title, String path) {
        // создаем объект для данных
        ContentValues cv = new ContentValues();
        // подключаемся к БД
        SQLiteDatabase db = getWritableDatabase();
        // подготовим значения для обновления
        cv.put(titleColumnName, title);
        cv.put(pathColumnName, path);
        // обновляем по id
        int updCount = db.update(RSSTableName, cv, "id = ?",
                new String[] {String.valueOf(id)});
        //Log.d(LOG_TAG, "updated rows count = " + updCount);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public void AddRSSList(String title, String path)
    {
        Log.v("DBHelper", "--- AddRSSList Start... ---");
        ContentValues cv = new ContentValues();
        cv.put( titleColumnName, title);
        cv.put( pathColumnName, path);
        // вставляем запись и получаем ее ID
        long rowID = getWritableDatabase().insert(RSSTableName, null, cv);
        close();
        Log.v("DBHelper", "--- AddRSSList Finish(id add row = " + rowID+ ") ---");
    }

    public void DeleteNote(Object id) {
        Log.v("DBHelper", "--- DeleteNote Start(delete id=" + id + ") ---");
        String where;
        if (id != null) where = " id=" + id;
        else where = null;
        int clearCount = getWritableDatabase().delete(RSSTableName, where, null);
        close();
        Log.v("DBHelper", "--- DeleteNote Finish(deleted rows count = " + clearCount + ") ---");
    }

    public ArrayList<RSSList> GetAllNotes()
    {
        Log.v("DBHelper", "--- GetAllNotes Start ---");
        ArrayList<RSSList> RSSLists=new ArrayList<RSSList>();
        Cursor c = getWritableDatabase().query(RSSTableName, null, null, null, null, null, null);
        boolean add;
        if (c.moveToFirst()) {
            do {
                RSSLists.add(new RSSList(c.getInt(c.getColumnIndex(idColumnName)), c.getString(c.getColumnIndex(titleColumnName))
                        , c.getString(c.getColumnIndex(pathColumnName))));
            } while (c.moveToNext());
        } else
            Log.v("DBHelper", "--- GetAllNotes(0 rows) ---");
        close();
        Log.v("DBHelper", "--- GetAllNotes Finish ---");
        return RSSLists;
    }
}
